class HereHandler
  def initialize
    @conn = Faraday::Connection.new 'https://router.hereapi.com'
  end

  def handle_distance_request(latitud1, longitud1, latitud2, longitud2)
    route = '/v8/routes?transportMode=car'
    origin = "origin=#{latitud1}%2C#{longitud1}"
    destination = "destination=#{latitud2}%2C#{longitud2}"
    return_sumarry = 'return=summary'
    api_key = "apikey=#{ENV['HERE_TOKEN']}"
    final_route = "#{route}&#{origin}&#{destination}&#{return_sumarry}&#{api_key}"
    @conn.get final_route
  end
end
