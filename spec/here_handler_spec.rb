require 'spec_helper'
require 'web_mock'

require "#{File.dirname(__FILE__)}/../lib/here_handler.rb"

def mock_connection
  ENV['HERE_TOKEN'] = '0'
  here_body = { "routes":
    [{ "id": '7eeff1e7-bae0-4a2a-870a-1875059ac9d4',
       "sections": [{ "summary": { "duration": 124,
                                   "length": 581,
                                   "baseDuration": 108 } }] }] }

  stub_request(:any, 'https://router.hereapi.com/v8/routes?transportMode=car&origin=0.00%2C0.00&destination=0.00%2C0.00&return=summary&apikey=0')
    .to_return(body: here_body.to_json, status: 200, headers: { 'Content-Length' => 3 })
end

describe 'HereHandler' do
  it 'distance request should have the correct format' do
    mock_connection

    handler = HereHandler.new
    response = handler.handle_distance_request('0.00', '0.00', '0.00', '0.00')
    expect(response).not_to equal nil
  end
end
